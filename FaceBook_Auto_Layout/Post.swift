//
//  File.swift
//  FaceBook_Auto_Layout
//
//  Created by Kimheang on 12/3/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import Foundation
class Post {
    var postTime: String
    var postLocation: String
    var statusText: String
    var imageName: String
    var numberOfLike: String
    var numberOfComment: String
    var numberOfShare: String
    var isTextPostOnly: Bool
    init(postTime: String, postLocation: String, statusText: String, imageName: String, numberOfLike: String, numberOfComment: String, numberOfShare: String, isTextPostOnly: Bool) {
        self.postTime = postTime
        self.postLocation = postLocation
        self.statusText = statusText
        self.imageName = imageName
        self.numberOfLike = numberOfLike
        self.numberOfComment = numberOfComment
        self.numberOfShare = numberOfShare
        self.isTextPostOnly = isTextPostOnly
    }
}
