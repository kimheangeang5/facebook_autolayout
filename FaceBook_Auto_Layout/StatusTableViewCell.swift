//
//  StatusTableViewCell.swift
//  FaceBook_Auto_Layout
//
//  Created by Kimheang on 11/30/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewCell: UIImageView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var postTime: UILabel!
    @IBOutlet weak var postLocation: UILabel!
    @IBOutlet weak var numberOfLike: UILabel!
    @IBOutlet weak var numberOfComments: UILabel!
    @IBOutlet weak var numberOfShares: UILabel!
    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var likePost: UIButton!
    @IBOutlet weak var sharePost: UIButton!
    var isLikePostClicked = true
    var isSharePostClicked = true
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewCell.layer.masksToBounds = false
        imageViewCell.layer.cornerRadius = imageViewCell.frame.height/2
        imageViewCell.clipsToBounds = true
        commentTextField.layer.masksToBounds = false
        commentTextField.layer.cornerRadius = commentTextField.frame.height/2
        commentTextField.clipsToBounds = false
        // TextFields Spacing
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 2.0))
        commentTextField.leftView = leftView
        commentTextField.leftViewMode = .always
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func prepareCell(post: Post) {
        postTime.text = post.postTime
        postLocation.text = post.postLocation
        numberOfLike.text = post.numberOfLike
        numberOfComments.text = post.numberOfComment
        numberOfShares.text = post.numberOfShare
        statusText.text = post.statusText
        if statusText.calculateMaxLines() < 2 {
            statusText.font = statusText.font.withSize(22)
        }
    }
    
    @IBAction func likePostClicked(_ sender: Any) {
        if isLikePostClicked{
            likePost.setBackgroundImage(UIImage(named: "like_post_blue"), for: .normal)
            isLikePostClicked = false
        }
        else{
            likePost.setBackgroundImage(UIImage(named: "like_post"), for: .normal)
            isLikePostClicked = true
        }
    }
    @IBAction func sharePostClicked(_ sender: Any) {
        if isSharePostClicked{
            sharePost.setBackgroundImage(UIImage(named: "share_post_blue"), for: .normal)
            isSharePostClicked = false
        }
        else{
            sharePost.setBackgroundImage(UIImage(named: "share_post"), for: .normal)
            isSharePostClicked = true
        }
    }
    @IBAction func postSetting(_ sender: Any) {
        
    }
}
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
