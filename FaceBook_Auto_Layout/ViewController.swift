//
//  ViewController.swift
//  FaceBook_Auto_Layout
//
//  Created by Kimheang on 11/30/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let cellSpacing: CGFloat = 4
    var posts = [Post]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        let post1 = Post(postTime: "Just now .", postLocation: "Phnom Penh", statusText: "Hello! My friend!", imageName: "", numberOfLike: "200 likes", numberOfComment: "390 Comments", numberOfShare: "1.5K Shares", isTextPostOnly: true)
        let post2 = Post(postTime: "8 hrs .", postLocation: "Kampot", statusText: "I'd recommend experimenting with the imageView in a plain view controller all by itself. This will let you tinker without wondering if your table cell is at fault. After you understand how UIImageView works and behaves, then move it into a tableViewcell.", imageName: "", numberOfLike: "486 likes", numberOfComment: "320 Comments", numberOfShare: "100 Shares", isTextPostOnly: true)
        let post3 = Post(postTime: "10 hrs .", postLocation: "Kandal", statusText: "I'd recommend experimenting with the imageViewI'd recommend experimenting with the imageView in a plain view controller all by itself. This will let you tinker without wondering if your table cell is at fault. After you understand how UIImageView works and behaves, then move it into a tableViewcell.", imageName: "sea", numberOfLike: "978 likes", numberOfComment: "300 Comments", numberOfShare: "100 Shares", isTextPostOnly: false)
        let post4 = Post(postTime: "Just now .", postLocation: "Kompong Soum", statusText: "My new house!!!", imageName: "house", numberOfLike: "260 likes", numberOfComment: "390 Comments", numberOfShare: "1.5K Shares", isTextPostOnly: false)
        let post5 = Post(postTime: "Just now .", postLocation: "Kompong Soum", statusText: "Feeling relax here!", imageName: "seaView", numberOfLike: "260 likes", numberOfComment: "390 Comments", numberOfShare: "1.7K Shares", isTextPostOnly: false)
        posts = [post1,post2,post3,post4,post5]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    var statusHeight: CGFloat = UITableView.automaticDimension
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if posts[indexPath.row].isTextPostOnly{
           let cell = Bundle.main.loadNibNamed("StatusTableViewCell", owner: self, options: nil)?.first as! StatusTableViewCell
            cell.prepareCell(post: posts[indexPath.row])
            return cell
        }
        else{
            let cell = Bundle.main.loadNibNamed("PictureTableViewCell", owner: self, options: nil)?.first as! PictureTableViewCell
            cell.prepareCell(post: posts[indexPath.row])
            statusHeight = CGFloat(cell.statusText.calculateMaxLines() * 65)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacing
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if posts[indexPath.row].isTextPostOnly == false{
            let image = UIImage(named: posts[indexPath.row].imageName)
            let aspect = (image?.size.width)! / (image?.size.height)!
            let cellHeight = (tableView.frame.width / aspect) + 163 + statusHeight
            return cellHeight
//            let currentImage = UIImage(named: posts[indexPath.row].imageName)
//            return (currentImage?.size.height)!
        }
        return UITableView.automaticDimension
    }
    
}

extension UIImage{
    func getCropRatio()-> CGFloat{
        let widthRatio = CGFloat(self.size.width/self.size.height)
        return widthRatio
    }
}

